#!/bin/bash
echo test.java.11.0.sh 1.0.0

# Run the tests
mvn test -e -B > output.txt

# Get the results from the output file
number_of_tests=$(grep -P -o "(?<=Tests run: )(\d+)" output.txt|head -1)

# The tests didn't run and something else is wrong
if [ -z "$number_of_tests" ]
then
    sh grade.sh error 'There was an error that prevented the tests from executing.'
    sed -n '/^\[ERROR\].*$/,$p' output.txt
    exit 1
fi

number_failed=$(grep -P -o "(?<=Failures: )(\d+)" output.txt|head -1)
number_errors=$(grep -P -o "(?<=Errors: )(\d+)" output.txt|head -1)
number_passed=$((number_of_tests - number_failed - number_errors))

# Call the script which writes a grading file and reports the result to the user.
sh grade.sh $number_passed $number_of_tests

# Print the portion of the output file that follows the search phrase
sed -n '/^-------------------------------------------------------$/,$p' output.txt
