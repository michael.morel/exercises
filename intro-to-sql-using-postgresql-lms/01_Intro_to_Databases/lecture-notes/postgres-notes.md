# Initial database setup (PostgreSQL)

1. Introduce `createdb`, and write the command for creating a database on the whiteboard in a place where it can stay during the course of the module:
	```
	createdb -U postgres UnitedStates
	```

2. Introduce `psql`, and write the command for running a SQL script on the whiteboard in a place where it can stay during the course of the module:
	```
	psql -U postgres -d UnitedStates -f UnitedStates-data.psql
	```

	- You'll find the `UnitedStates-data.psql` file in the `lecture-student` folder of the lecture.

3. Explain that you can run the script again to reset the database.

4. Introduce the students to DbVisualizer, and show them how to create a new connection to the database they just created and populated with data.

## Notes

- The Postgres server may not be running by default on the student laptops. To run it by default, you need to start PowerShell as an Administrator and enter the following command:
	```
	Set-ExecutionPolicy Bypass -Scope Process -Force;~\startpostgresql.ps1
	```

- `postgres1` is the default password for the `postgres` user.
- Don't go through the SQL in the setup script today. If they ask, let students know you'll cover creating databases later in the module.
- Make sure students understand that the `.psql` file isn't the database. It's a script that creates the database.
  - The `.psql` extension indicates it uses SQL syntax that's specific to PostgreSQL. They may also see `.sql` in their careers, especially if they end up working with a different RDBMS.
- Make sure students understand that DbVisualizer isn't the database. It's a UI for the PostgreSQL server application that runs in the background all the time. Usually, it would run on a separate computer.